<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
        $this->load->helper('url');

		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer', array('type' => ''));
	}

	public function registration()
	{
        $this->load->helper('url');

		$this->load->view('header');
		$this->load->view('registration');
		$this->load->view('footer', array('type' => 'registration'));
	}

    public function complete_registration()
    {
        $this->load->model('Registration');

        // make values manageable to check if empty
        $registration['school'] = $this->input->post('school');
        $registration['type'] = $this->input->post('type');
        $registration['coach_name'] = $this->input->post('coach_name');
        $registration['coach_email'] = $this->input->post('coach_email');
        
        $student1 = $this->input->post('student1');        
        $student2 = $this->input->post('student2');        
        $student3 = $this->input->post('student3');        
        $student4 = $this->input->post('student4');        

        $registration['students'] = array($student1, $student2, $student3, $student4);

        // if required fields are filled out, register team
        if ($registration['school'] != '' && $registration['type'] != '' && $registration['coach_name'] != '' && $registration['coach_email'] != '' && $student1 != '')
        {
            $team_id = $this->Registration->register($registration);

            // display team information to verify registration
            $this->info('team', $team_id);        
        }

        else
        {
            // need error message
            $this->registration();
        }
    }


	public function grading_login()
	{
        // if already logged in, redirect
        $session = $this->session->userdata('grading');
        if ($session)
            $this->grading_type();

        // if not, display login page
        else
        {
            $this->load->helper('url');

	    	$this->load->view('header');
	    	$this->load->view('grading_login');
	    	$this->load->view('footer', array('type' => 'grading'));
	    }
	}

	public function grading_type()
	{
        $this->load->helper('url');

        $this->load->model('Login');
        $this->load->model('Grading');

        $session = $this->session->userdata('grading');

        // if not already logged in, check inputted password
        if (!$session)        
            $login = $this->Login->login('grading');

        // if already logged in or correct password, display grading page and login user
        if ($login || $session)
        {
            $this->session->set_userdata('grading', 'true');

    		$this->load->view('header');
	    	$this->load->view('grading_type');
	    	$this->load->view('footer', array('type' => 'grading'));
	    }
	    
	    // if not already logged in and password incorrect, redirect
	    else
	        $this->grading_login();
	}

	public function grading_list($score_type)
	{
        // if not already logged in, redirect
        $session = $this->session->userdata('grading');
        if (!$session)
            $this->grading_login();
        
        else
        {
            $this->load->model('Grading');

            // get ungraded and graded tests to display
            $ungraded = $this->Grading->get_ungraded($score_type);
            $graded = $this->Grading->get_graded($score_type);
    
        	$this->load->view('header');
	        $this->load->view('grading_list', array('score_type' => $score_type, 'ungraded' => $ungraded, 'graded' => $graded));
	        $this->load->view('footer', array('type' => 'grading'));
	    }
	}

	public function grading_main($score_type, $id)
	{
        // if not already logged in, redirect
        $session = $this->session->userdata('grading');
        if (!$session)
            $this->grading_login();

        else
        {
            $this->load->helper('url');
            $this->load->model('Grading');
            
            // when being graded, place test in regrade section so graders won't overlap
            $this->Grading->lockout($score_type, $id);

	    	$this->load->view('header');
	    	$this->load->view('grading_main', array('score_type' => $score_type, 'id' => $id));
	    	$this->load->view('footer', array('type' => 'grading'));
	    }
	}
	
	public function complete_grading($score_type, $id)
	{
        $session = $this->session->userdata('grading');
        $submit = $this->input->post('submit');

        // determine which info is shown
        if ($score_type != 'team_score')
            $type = 'student';
        else
            $type = 'team';            

        // if not already logged in, redirect
        if (!$session)
            $this->grading_login();

        else if ($submit)
        {
            $this->load->model('Grading');
        
            // number of questions corresponding to test type
            $score = 0;
            if ($score_type == 'sprint_score')
                $n = 30;
                            
            else if ($score_type == 'target_score')
                $n = 8;
                            
            else
                $n = 10;

            // iterate over questions, score correct answers
            for ($i = 1; $i < $n + 1; $i++)
            {
                $question = $this->input->post($i);
                if ($question == 'true')
                    $score++;
            }
            
            $this->Grading->input_grade($id, $score_type, $score);
	    }
	    
	    $this->info($type, $id);
	}

	public function rankings_login()
	{
        // if already logged in, redirect
        $session = $this->session->userdata('rankings');
        if ($session)
            $this->rankings('student');

        else
        {
            $this->load->helper('url');

	    	$this->load->view('header');
	    	$this->load->view('rankings_login');
	    	$this->load->view('footer', array('type' => 'rankings'));
	    }
	}

	public function rankings($type)
	{
        $this->load->helper('url');

        $this->load->model('Login');
        $this->load->model('Rankings');

        // if not already logged in, check inputted password
        $session = $this->session->userdata('rankings');        
        if (!$session)
            $login = $this->Login->login('rankings');

        // if already logged in or correct password, display rankings page and login user        
        if ($login || $session)
        {
            $this->session->set_userdata('rankings', 'true');

            // get specific type of rankings
            if ($type == 'student')
                $rankings = $this->Rankings->get_student();
            
            else
                $rankings = $this->Rankings->get_team();
    
    		$this->load->view('header');
    		$this->load->view('rankings', array('rankings' => $rankings, 'type' => $type));
    		$this->load->view('footer', array('type' => 'rankings'));
    
	    }
	    
	    // if not already logged in and password incorrect, redirect	    
	    else
	        $this->rankings_login();

	}

	public function info($type, $id)
	{
        $this->load->helper('url');

        $this->load->model('Info');

        // get specific type of information
        if ($type == 'student')
            $info = $this->Info->student_info($id);
        
        else
            $info = $this->Info->team_info($id);            

		$this->load->view('header');
		$this->load->view('info', array('info' => $info, 'type' => $type));
		$this->load->view('footer', array('type' => ''));
	}

	public function resources_type()
	{
        $this->load->helper('url');

		$this->load->view('header');
		$this->load->view('resources_type');
		$this->load->view('footer', array('type' => 'resources'));
	}

	public function resources($type)
	{
        $this->load->helper('url');
        
        $this->load->model('Resources');

        // get specific type of resources
        $resources = $this->Resources->get_resources($type);

		$this->load->view('header');
		$this->load->view('resources', array('resources' => $resources, 'type' => $type));
		$this->load->view('footer', array('type' => 'resources'));
	}
}
