<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	public function registration()
	{
        $this->load->helper('url');

		$this->load->view('header');
		$this->load->view('registration/registration');
		$this->load->view('footer', array('type' => 'registration'));
	}

    public function complete_registration()
    {
        $this->load->model('Registration');

        $registration['school'] = $this->input->post('school');
        $registration['type'] = $this->input->post('type');
        $registration['coach_name'] = $this->input->post('coach_name');
        $registration['coach_email'] = $this->input->post('coach_email');        

        $student1 = $this->input->post('student1');
        $student2 = $this->input->post('student2');
        $student3 = $this->input->post('student3');
        $student4 = $this->input->post('student4');
        $registration['students'] = array($student1, $student2, $student3, $student4);

        if ($registration['school'] != '' && $registration['type'] != '' && $registration['coach_name'] != '' && $registration['coach_email'] != '' && $student1 != '')
        {
            $this->Registration->register($registration);
        
    		$this->load->view('header');
	    	$this->load->view('registration/complete_registration');
	    	$this->load->view('footer', array('type' => 'registration'));
        }

        else
        {
            // need error message
            $this->registration();
        }
    }
    
}
