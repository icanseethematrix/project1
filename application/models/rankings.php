<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rankings extends CI_Model {

	public function get_student()
	{
        // get students who have been graded
        $query = $this->db->query("SELECT student_id, student_name, sprint_score + 2 * target_score FROM student NATURAL JOIN student_score WHERE sprint_score IS NOT NULL AND target_score IS NOT NULL ORDER BY sprint_score + 2 * target_score DESC;");
        
	    $rankings = $query->result_array();
	    
	    // make indexes generic so as to use same view
        foreach ($rankings as &$ranking)
        {
            $ranking['id'] = $ranking['student_id'];
            $ranking['display'] = $ranking['student_name'] . ': ' . $ranking['sprint_score + 2 * target_score'];
        }
        
        return $rankings;
	}

	public function get_team()
	{
        // get teams who have been graded
        $query = $this->db->query("SELECT team_id, school, type, 0.25 * SUM(sprint_score + 2 * target_score) + 2 * team_score FROM student_score NATURAL JOIN student_team NATURAL JOIN team NATURAL JOIN team_score WHERE team_score IS NOT NULL AND sprint_score 
                                   IS NOT NULL AND target_score IS NOT NULL GROUP BY team_id ORDER BY 0.25 * SUM(sprint_score + 2 * target_score) + 2 * team_score DESC");
                        
	    $rankings = $query->result_array();
	    
	    // make indexes generic so as to use same view
	    foreach ($rankings as &$ranking)
        {
            $ranking['id'] = $ranking['team_id'];
            $ranking['display'] = $ranking['school'] . ' ' . $ranking['type'] . ': ' . $ranking['0.25 * SUM(sprint_score + 2 * target_score) + 2 * team_score'];
        }
        
        return $rankings;
	}
}

?>
