<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Model {

	public function login($type)
	{
        // get inputted password
        $password = $this->input->post("password");
        
        // compare to password for type of login
        if ($type == 'rankings')
            return $password == 'euler';
        else
            return $password == 'mathcounts';
	}

}

?>
