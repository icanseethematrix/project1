<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Model {

    public function register($registration)
    {
        // make values easier to handle
        $school = $registration["school"];
        $type = $registration["type"];
        $coach_name = $registration["coach_name"];
        $coach_email = $registration["coach_email"];
        
        // insert team info into team table
        $query = $this->db->query("INSERT INTO team (school, type, coach_name, coach_email) VALUES ('$school', '$type', '$coach_name', '$coach_email');");

        // insert null team score
        $team_id = $this->db->insert_id();
        $query = $this->db->query("INSERT INTO team_score (team_id) VALUES ($team_id);");        
        
        foreach ($registration["students"] as $student)
        {
            // add students to database, initialize scores to null
            if ($student != '')
            {
                $query = $this->db->query("INSERT INTO student (student_name) VALUES ('$student');");
                $student_id = $this->db->insert_id();
                $query = $this->db->query("INSERT INTO student_team (student_id, team_id) VALUES ($student_id, $team_id)");
                $query = $this->db->query("INSERT INTO student_score (student_id) VALUES ($student_id)");
            }
        }
    
        return $team_id;
    }

}

?>
