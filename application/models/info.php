<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends CI_Model {

	public function student_info($id)
	{
        // get all student information from database
        $query = $this->db->query("SELECT * FROM student NATURAL JOIN student_score NATURAL JOIN student_team NATURAL JOIN team WHERE student_id = $id;");
        
	    $info = $query->result_array();

	    $sprint = '';
	    $target = '';
	    $total = '';
	    
        // don't display sprint score if ungraded
	    if(!empty($info[0]['sprint_score']))
            $sprint = 'Sprint Score:<br>' . $info[0]['sprint_score'] . '<br><br>';
            
        // don't display target score if ungraded
	    if(!empty($info[0]['target_score']))
            $target = 'Target Score:<br>' . $info[0]['target_score'] . '<br><br>';

        // don't display total score if anything is ungraded
	    if(!empty($info[0]['sprint_score']) && !empty($info[0]['target_score']))
        {
            $total = $info[0]['sprint_score'] + 2 * $info[0]['target_score'];
            $total = 'Total Score:<br>' . $total . '<br><br>';
        }
        
        // information to be displayed
	    $display = 'Student:<br>' . 
	                $info[0]['student_name'] . '<br><br>' . 
	                $sprint . $target . $total . 'Team: <br><br>
	                <ul data-role="listview">
                    <li><a href=' . site_url() . '/main/info/team/' . $info[0]['team_id'] . '>' . $info[0]['school'] . " " . $info[0]['type'] . '</a></li>
                    </ul>';
        
        return $display;
	}

	public function team_info($id)
	{
        // get all team information from database
        $query = $this->db->query("SELECT * FROM team NATURAL JOIN team_score NATURAL JOIN student_team NATURAL JOIN student NATURAL JOIN student_score WHERE team_id = $id;");
                
	    $info = $query->result_array();

        // initialize values
        $team = $info[0]['team_score'];
	    $total = 2 * $team;
	    $list = '';
        $scored = 'true';

        // if team score empty, don't display it
	    if(empty($team))
            $team = '';
        
        else
            $team = 'Team Score:<br>' . $team . '<br><br>';        
        
        foreach ($info as $student)
        {
            // add links to info for students on team and add their scores
            $list .= '<li><a href=' . site_url() . '/main/info/student/' . $student['student_id'] . '>' . $student['student_name'] . '</a></li>';
            $total += 0.25 * $student['sprint_score'] + 0.5 * $student['target_score'];
            
            // if any elements ungraded, don't display results
            if(empty($student['sprint_score']) || empty($student['target_score']))
                $scored = 'false';
        }
        
        // if everything graded, display total score
        if ($scored == 'true')
            $total = 'Total Score:<br>' . $total . '<br><br>';

        else
            $total = '';

        // information to be displayed
	    $display = 'Team:<br>' .
	                $info[0]['school'] . " " . $info[0]['type'] . 
	                '<br><br>Coach:<br>' . $info[0]['coach_name'] . '<br>' .
	                $info[0]['coach_email'] . '<br><br>' . 
	                $team . $total .	                
                    'Students:<br><br>
	                <ul data-role="listview">' . 
	                $list . 
	                '</ul>';
        
        return $display;
	}
}
?>
