<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resources extends CI_Model {

	public function get_resources($type)
	{
        // get desired directory
	    $directory = "../html/resources/" . $type;
        $handler = opendir($directory);
        
        // iterate over files and add to array
        while ($resource = readdir($handler))
        {
            if ($resource != "." && $resource != "..")
                $resources[] = $resource;
        }

        closedir($handler);

        return $resources;
	}

}

?>
