<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grading extends CI_Model {

	public function lockout($score_type, $id)
	{
        // set sprint or target score to 0, placing that test in the "regrade" category so others won't simultaneously grade
        if ($score_type != 'team_score')
            $query = $this->db->query("UPDATE student_score SET $score_type = 0 WHERE student_id = $id;");
        
        // set team score to 0, placing that test in the "regrade" category so others won't simultaneously grade
        else
            $query = $this->db->query("UPDATE team_score SET team_score = 0 WHERE team_id = $id;");        
	}

	public function get_ungraded($score_type)
	{
        // select ungraded sprint or target tests
        if ($score_type != "team_score")
            $query = $this->db->query("SELECT student_id, student_name FROM student WHERE student_id IN (SELECT student_id FROM student_score WHERE $score_type IS NULL) ORDER BY student_name ASC;");

        // select ungraded team tests
        else
            $query = $this->db->query("SELECT team_id, school, type FROM team WHERE team_id IN (SELECT team_id FROM team_score WHERE team_score IS NULL) ORDER BY school ASC;");            
        
	    return $query->result_array();
	}

	public function get_graded($score_type)
	{
        // select graded sprint or target tests
        if ($score_type != "team_score")
            $query = $this->db->query("SELECT student_id, student_name FROM student WHERE student_id IN (SELECT student_id FROM student_score WHERE $score_type IS NOT NULL) ORDER BY student_name ASC;");

        // select graded team tests
        else
            $query = $this->db->query("SELECT team_id, school, type FROM team WHERE team_id IN (SELECT team_id FROM team_score WHERE team_score IS NOT NULL) ORDER BY school ASC;");            

	    return $query->result_array();
	}

	public function input_grade($id, $score_type, $score)
	{
        // input sprint or target score
        if ($score_type != "team_score")
            $query = $this->db->query("UPDATE student_score SET $score_type = $score WHERE student_id = $id;");
        
        // input team score
        else
            $query = $this->db->query("UPDATE team_score SET team_score = $score WHERE team_id = $id;");
	}
}

?>
