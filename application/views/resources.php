<div data-role="page" id="resources">

	<div data-role="header" data-position="fixed">
		<h1>Resources</h1>
	</div><!-- /header -->
        
    <ul data-role="listview">

        <? 
            // link to files, if present
            if (!empty($resources))           
                foreach ($resources as $resource)
                { 
        ?>
                    <li><a rel="external" href="../../../resources/<?= $type ?>/<?= $resource ?>"><?= $resource ?> </a></li>
        <?
                }   
            else        
            {
        ?>
            <li> Nothing to display! </li>
        <? } ?>

    </ul>
