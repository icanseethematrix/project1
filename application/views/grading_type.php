<div data-role="page" id="grading_type">

	<div data-role="header" data-position="fixed">
		<h1>Grading Mode</h1>
	</div><!-- /header -->
        
    <div data-role = "content">
        <ul data-role="listview">
            <li><a href="/index.php/main/grading_list/sprint_score/">Sprint</a></li>
            <li><a href="/index.php/main/grading_list/target_score/">Target</a></li>
            <li><a href="/index.php/main/grading_list/team_score/">Team</a></li>
        </ul>
    </div>
