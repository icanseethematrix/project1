<div data-role="page" id="rankings">

	<div data-role="header" data-position="fixed">
		<h1>Rankings</h1>
        <div data-role="navbar">
            <ul>
                <li><a href="<?= site_url() ?>/main/rankings/student/">By Student</a></li>
                <li><a href="<?= site_url() ?>/main/rankings/team/">By Team</a></li>
            </ul>
        </div><!-- /navbar -->
   	</div><!-- /header -->

    <div data-role="content">
        <ul data-role="listview">
        <? 
            // numerically rank entries
            $i = 0;
                
            foreach ($rankings as $ranking) 
            { 
                $i++;
        ?>                
            <li><a href="<?= site_url() ?>/main/info/<?= $type ?>/<?= $ranking['id'] ?>"><?= $i . ". " . $ranking['display'] ?></a></li>
            
        <? } ?>
            
        </ul>
    </div><!-- /content -->

