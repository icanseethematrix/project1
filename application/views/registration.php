
<div data-role="page" id="registration">

	<div data-role="header" data-position="fixed">
		<h1>Register</h1>
	</div><!-- /header -->

    <div data-role="fieldcontain">
        <form action="<?= site_url(); ?>/main/complete_registration/" method="post">
            <label for="school" class="ui-hidden-accessible"> School:</label>
            <input type="text" name="school" value="" placeholder = "School Name"/>
            
            <fieldset data-role="controlgroup">
                <legend>Choose type:</legend>
                    <input type="radio" id="type-choice-1" name="type" value="V" checked="checked"/>
                    <label for="type-choice-1">Varsity</label>

                    <input type="radio" id="type-choice-2" name="type" value="JV"/>
                    <label for="type-choice-2">Junior Varsity</label>
            </fieldset>         
            
            <label> Coach: </label><br/>
            
            <label for="coach-name" class="ui-hidden-accessible"> Coach Name:</label>
            <input type="text" name="coach_name" value="" placeholder = "Coach Name"/>
            <label for="coach-email" class="ui-hidden-accessible"> Coach Email:</label>
            <input type="text" name="coach_email" value="" placeholder = "Coach Email"/>
            
            <br/><br/>
            <label> Students: </label><br/>
            
            <label for="student-name-1" class="ui-hidden-accessible"> Student 1:</label>
            <input type="text" name="student1" value="" placeholder = "Student 1"/>
            <label for="student-name-2" class="ui-hidden-accessible"> Student 2:</label>
            <input type="text" name="student2" value="" placeholder = "Student 2"/>
            <label for="student-name-3" class="ui-hidden-accessible"> Student 3:</label>
            <input type="text" name="student3" value="" placeholder = "Student 3"/>
            <label for="student-name-4" class="ui-hidden-accessible"> Student 4:</label>
            <input type="text" name="student4" value="" placeholder = "Student 4"/>
            
            <input data-role="button" type="submit" value="Register"> 
        </form>
    </div>
