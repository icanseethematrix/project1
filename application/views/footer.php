<div data-role="footer" data-position = "fixed">
    <div data-role="navbar" class = "main-navbar">
        <ul>
            <?
                // initialize values
                $html = 'data-transition = "pop" class = "ui-btn-active ui-state-persist"';
                $registration = '';
                $grading = '';
                $rankings = '';
                $resources = '';
                            
                // attach css style to current page            
                if ($type == 'registration')
                    $registration = $html;
                else if ($type == 'grading')
                    $grading = $html;
                else if ($type == 'rankings')
                    $rankings = $html;                
                else if ($type == 'resources')
                    $resources = $html;                
            ?>

            <li><a id ="navbar-register" data-icon="custom" href="/index.php/main/registration" <?= $registration ?> >Register</a></li>
            <li><a id ="navbar-grading" data-icon="custom" href="/index.php/main/grading_login" <?= $grading ?> >Grading</a></li>
            <li><a id ="navbar-rankings" data-icon="custom" href="/index.php/main/rankings/student"  <?= $rankings ?> >Rankings</a></li>
            <li><a id="navbar-resources" data-icon="custom" href="/index.php/main/resources_type" <?= $resources ?> >Files</a></li>
        </ul>
    </div><!-- /navbar --> 
</div><!-- /footer -->

</div><!-- /page -->
    
</body>

</html>
