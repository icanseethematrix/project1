<div data-role="page" id="grading-main">
	<div data-role="header" data-position="fixed">
		<h1>Grading</h1>
	</div><!-- header -->
  
    <div data-role="content">
        <div data-role="fieldcontain">
            <form action="<?= site_url() ?>/main/complete_grading/<?= $score_type ?>/<?= $id ?>" method="post">            
                <? 
                    // number of questions corresponding to test type
                    if ($score_type == "sprint_score")
                        $n = 30;

                    else if ($score_type == "target_score")
                        $n = 8;

                    else
                        $n = 10;

                    for ($i = 1; $i < $n + 1; $i++) { 

                ?>
                <label for="flip-min"><b>Question <?= $i ?></b></label>
                <select name="<?= $i ?>" data-role="slider">
                    <option value="false">Incorrect</option>
                    <option value="true">Correct</option>
                </select>
                <br><br>
                <? } ?>
                <input data-role="button" name="submit" type="submit" value="Submit"> 
            </form>
        </div>
    </div><!-- content -->
