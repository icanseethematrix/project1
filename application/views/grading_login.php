<div data-role="page" id="grading-login">

	<div data-role="header" data-position="fixed">
		<h1>Login</h1>
	</div><!-- /header -->
    
    <div data-role="fieldcontain">
        <form action="<?= site_url() ?>/main/grading_type/" method="post">
            <label for="password" class="ui-hidden-accessible"> Password:</label>

            <strong>Please enter the password:</strong>
            <br>
            <input type="text" id="password" value="" name = "password"/>
            
            <input data-role="button" type="submit" value="Login"> 
        </form>
    </div>
