<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>MathCounts Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 

    <!-- Adds javascript libraries like jquery -->
    <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>

    <style>	
        .main-navbar .ui-btn .ui-btn-inner { padding-top: 40px !important; }
        .main-navbar .ui-btn .ui-icon { width: 30px!important; height: 30px!important; margin-left: -15px !important; box-shadow: none!important; -moz-box-shadow: none!important; -webkit-box-shadow: none!important; -webkit-border-radius: 0 !important; border-radius: 0 !important; }

        #navbar-register .ui-icon { background:  url(../../../img/179-notepad.png) 50% 50% no-repeat; background-size: 22px 28px; }
        #navbar-grading .ui-icon { background:  url(../../../img/117-todo.png) 50% 50% no-repeat; background-size: 18px 19px;  }
        #navbar-rankings .ui-icon { background:  url(../../../img/17-bar-chart.png) 50% 50% no-repeat;  background-size: 29px 24px; }
        #navbar-resources .ui-icon { background:  url(../../../img/96-book.png) 50% 50% no-repeat;  background-size: 18px 26px; }
    </style>

    <!-- Linking style sheets from external libraries -->   
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
    <link rel="stylesheet" href="../../../themes/orange.min.css" />

</head>

<body>
