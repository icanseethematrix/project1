<div data-role="page" id="grading-list">
	<div data-role="header" data-position="fixed">
		<h1>
		    <?
		        // display corresponding header for grading mode
		        if ($score_type == "sprint_score")
		            echo 'Sprint';
		            
		        else if ($score_type == "target_score")
		            echo 'Target';
		            
		        else
		            echo 'Team';
		    ?>
		</h1>
	</div><!-- /header -->
        
    <div data-role = "content">
        <strong>Tests to be graded:</strong><br><br>
        <ul data-role="listview">
        <?                
            // allow for the creation of generic links below
            $type = '';
            if ($score_type != 'team_score')
            {
                $id = 'student_id';
                $name = 'student_name';
            }
                
            else
            {
                $id = 'team_id';
                $name = 'school';
            }
        
            // iterate over ungraded tests
            foreach ($ungraded as $test)
            { 
                if ($score_type == 'team_score')
                    $type = ' ' . $test['type'];
        ?>
            <li><a href="/index.php/main/grading_main/<?= $score_type ?>/<?= $test[$id] ?>"><?= $test[$name] . $type ?></a></li>
        <? } ?>
        </ul>
        <br><br><strong>Tests to be regraded, if necessary:</strong><br><br>

        <ul data-theme="b" data-role="listview">
        <?
            // iterate over graded tests
            foreach ($graded as $test)
            {
                if ($score_type == 'team_score')
                    $type = ' ' . $test['type'];
        ?>
            <li><a href="/index.php/main/grading_main/<?= $score_type ?>/<?= $test[$id] ?>"><?= $test[$name] . $type ?></a></li>
        <? } ?>
        </ul>
    </div>
