<div data-role="page" id="resources_type">

	<div data-role="header" data-position="fixed">
		<h1>Resources</h1>
	</div><!-- /header -->
        
    <ul data-role="listview">
        <li><a href="/index.php/main/resources/past_exams">Past exams</a></li>            
        <li><a href="/index.php/main/resources/warmups">Warmups</a></li>
        <li><a href="/index.php/main/resources/workouts">Workouts</a></li>
        <li><a href="/index.php/main/resources/handbook">Handbook</a></li>
    </ul>
