<div data-role="page" id="ranking-login">

	<div data-role="header" data-position="fixed">
		<h1>Login</h1>
	</div><!-- /header -->
	
    <div data-role="fieldcontain">
        <form action="<?= site_url() ?>/main/rankings/" method="post">
            <label for="password" class="ui-hidden-accessible"> Password:</label>

            <strong>Please enter the password (will be announced once all entries have been graded): </strong>
            <br>
            <input type="text" id="password" value="" name = "password"/>
            <input data-role="button" type="submit" value="Login"> 
        </form>
    </div>
