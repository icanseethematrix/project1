-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2012 at 05:41 AM
-- Server version: 5.5.19
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jharvard_project1`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_name` varchar(255) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `student_name`) VALUES
(26, 'Joseph Botros'),
(27, 'Mark Botros'),
(28, 'Michael Mardini'),
(29, 'Nick Posey'),
(34, 'Neelay Purohit'),
(35, 'Alex Barksdale'),
(36, 'Andrew Keiser'),
(37, 'Ian Francis'),
(38, 'Simon Q. Linus'),
(39, 'Penny H. Peters'),
(40, 'Henry T. M. Loveall'),
(42, 'Matt Polowski'),
(43, 'Jonathan Ji'),
(44, 'Daisy Johnson'),
(45, 'Mark Io'),
(46, 'Max Europa'),
(47, 'David Davidenko'),
(48, 'Kristen Rogers'),
(49, 'Kate Beckinsale'),
(50, 'Winston Churchill');

-- --------------------------------------------------------

--
-- Table structure for table `student_score`
--

CREATE TABLE IF NOT EXISTS `student_score` (
  `student_id` int(10) unsigned NOT NULL,
  `sprint_score` tinyint(2) unsigned DEFAULT NULL,
  `target_score` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `sprint_score` (`sprint_score`,`target_score`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_score`
--

INSERT INTO `student_score` (`student_id`, `sprint_score`, `target_score`) VALUES
(38, NULL, NULL),
(39, NULL, NULL),
(42, NULL, NULL),
(46, NULL, NULL),
(40, 0, NULL),
(43, 0, NULL),
(44, 0, NULL),
(45, 0, 0),
(35, 5, 0),
(50, 17, 4),
(37, 18, 4),
(36, 19, 3),
(27, 20, 6),
(47, 20, 6),
(49, 21, 3),
(48, 21, 8),
(28, 22, 5),
(34, 25, 3),
(26, 26, 6),
(29, 27, 7);

-- --------------------------------------------------------

--
-- Table structure for table `student_team`
--

CREATE TABLE IF NOT EXISTS `student_team` (
  `student_id` int(10) unsigned NOT NULL,
  `team_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`student_id`,`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_team`
--

INSERT INTO `student_team` (`student_id`, `team_id`) VALUES
(26, 12),
(27, 12),
(28, 12),
(29, 12),
(34, 15),
(35, 15),
(36, 15),
(37, 15),
(38, 16),
(39, 16),
(40, 16),
(42, 17),
(43, 17),
(44, 17),
(45, 18),
(46, 18),
(47, 19),
(48, 19),
(49, 19),
(50, 19);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `team_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school` varchar(255) NOT NULL,
  `type` enum('JV','V') NOT NULL,
  `coach_name` varchar(255) NOT NULL,
  `coach_email` varchar(255) NOT NULL,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `school` (`school`,`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `school`, `type`, `coach_name`, `coach_email`) VALUES
(12, 'Honey Creek Middle School', 'V', 'Bob Fischer', 'rtf@vigoschools.org'),
(15, 'Honey Creek Middle School', 'JV', 'Bob Fischer', 'rtf@vigoschools.org'),
(16, 'Monta Vista', 'V', 'Daniel Ki', 'danielrki@gmail.com'),
(17, 'International School', 'V', 'Ibrahim Sheikh', 'isheikh@ilovemath.com'),
(18, 'Central Academy', 'V', 'Mary Lamb', 'mary@lamb.com'),
(19, 'Math Masters Montessori', 'V', 'Margaret Mathematica', 'maggiemath@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `team_score`
--

CREATE TABLE IF NOT EXISTS `team_score` (
  `team_id` int(11) NOT NULL,
  `team_score` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`team_id`),
  KEY `team_score` (`team_score`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_score`
--

INSERT INTO `team_score` (`team_id`, `team_score`) VALUES
(16, NULL),
(17, NULL),
(18, NULL),
(12, 1),
(19, 2),
(15, 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
